import serial
import time
import csv


ser = serial.Serial('/dev/ttyUSB0', 9400, timeout=1)   # open serial port
time.sleep(1)
ser.flushInput()
print(ser.port)         # check which port was really used
ser.write(b'wakeywakey!\r\n')
print(ser.readline())
print(ser.readline())

with open("/home/pi/Desktop/cron_test/SBE19p_test_data2.csv","a") as f:
    writer = csv.writer(f,delimiter=",")
    writer.writerow([])
    writer.writerow(["Next SBE19p profile"])


ser.write(b'startnow\r\n')
print(ser.readline())
time.sleep(3)

print("bytes to read: ",ser.inWaiting())
print(ser.readline())


for i in range(15):
    time.sleep(0.1)
    if ser.inWaiting()!=0:
        data=ser.readline()
        print("Waiting",data)
        with open("/home/pi/Desktop/cron_test/SBE19p_test_data2.csv","a") as f:
            writer = csv.writer(f,delimiter=",")
            writer.writerow([time.asctime(time.gmtime(time.time())),data])
        

ser.write(b'stop\r\n')
print(ser.readline())
print(ser.readline())
ser.write(b'QS\r\n')
print(ser.readline())
ser.close()
print(ser)
