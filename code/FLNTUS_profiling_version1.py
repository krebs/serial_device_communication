import serial
import time
import csv


ser = serial.Serial('/dev/ttyUSB1', 19200, timeout=1)   # open serial port
ser.flushInput()
time.sleep(1)
print(ser.port)         # check which port was really used
ser.write(b'$run\r\n')
time.sleep(2)
print(ser.readline())


#start measuring
for i in range(5):    
    x=ser.readline()
    print("Dataline: ",x)
    y=ser.inWaiting()
    print("Bytes in waiting: ",y)
    
    with open("test_data.csv","a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([time.asctime(time.gmtime(time.time())),x])
    
#interrupt measurement
for i in range(10):
    ser.write(b'!')
    #time.sleep(0.025) #if the 'stop' command does not work uncomment this line

#check for remaining data in buffer
data_left=False
if ser.inWaiting()!=0:
    print("Noch",ser.inWaiting(),"Bytes zu lesen")
    data_left=True
    
#read out rest of buffer   
while data_left==True:   
    x=ser.readline()
    print("Readline: ",x)
    with open("test_data.csv","a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([time.asctime(time.gmtime(time.time())),x])
    y=ser.inWaiting()
    print("Bytes in waiting: ",y)
    if y==0:
        data_left=False
        
#kill measurement        
time.sleep(1)
for i in range(10):
    ser.write(b'!')
    #time.sleep(0.025) #if the 'stop' command does not work uncomment this line

#read out FLNTUS steup parameters
while ser.inWaiting()!=0:
    k=ser.readline()
    print(k)
    with open("test_parameter.csv","a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([time.asctime(time.gmtime(time.time())),k])

ser.close()             # close port
print(ser)
